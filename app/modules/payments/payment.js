const { generateResNum, create } = require("./model");
const { status, types, getTypesByWord } = require("./paymentsType");
const cash = require("./handler/cash");
const online = require("./handler/online");

const methods = {
  cash,
  online
};

exports.doPayments = async (order, paymentMethod) => {
  const method = methods[paymentMethod];

  const paymentModel = {
    order_id: order.id,
    amount: order.order_total,
    res_num: generateResNum(),
    gateway: " ",
    status: status.PENDING,
    type: types[paymentMethod]
  };

  const payment = await create(paymentModel);

  const result = await method.pay(payment);
  return result;
};

exports.verifyPayment = async (payments, params) => {
  const method = getTypesByWord[payments.type];
  const verify = await methods[method].verify(payments, params);
  return verify;
};
