exports.status = {
  PENDING: 0,
  PAID: 1
};

exports.types = {
  cash: 0,
  online: 1
};

exports.getTypesByWord = {
  0: "cash",
  1: "online"
};
