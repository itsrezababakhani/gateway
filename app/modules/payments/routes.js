const express = require("express");
const paymentController = require("./controller");
const router = express.Router();

router.post("/:order_hash", paymentController.index);
router.post("/verify/:payment_hash", paymentController.verifyPayments);
router.get("/verify/:payment_hash", paymentController.verifyPayments);

module.exports = router;
