const zarinPalClient = require("zarinpal-checkout");
const zarinaPalKey = "adf540fa-18c3-11e9-a86b-005056a205be";
const zarinpal = zarinPalClient.create(zarinaPalKey, false);
exports.paymentRequest = async payment => {
  const gatewayResult = await zarinpal.PaymentRequest({
    Amount: payment.amount, // In Tomans
    CallbackURL: `https://localhost:3000/payments/verify/${payment.hash}`,
    Description: "A Payment from Node.JS"
  });

  if (gatewayResult.status === 100) {
    return {
      success: true,
      mustRedirect: true,
      redirectUrl: gatewayResult.url
    };
  }
  console.log("error");
};

exports.verify = async (payments, params) => {
  const response = await zarinpal.PaymentVerification({
    Amount: payments.amount, // In Tomans
    Authority: params.query.Authority
  });

  if (response.status === -21) {
    console.log(response);
    return {
      success: false
    };
  }

  return {
    success: true,
    ref_num: response.RefID
  };
};
