const webService =
  "https://sep.shaparak.ir/payments/referencepayment1.asmx?WSDL";
const MID = "10372149";

exports.paymentRequest = async payment => {
  return {
    success: true,
    mustRenderView: true,
    viewPath: "gateways/saman",
    viewParams: {
      Amount: payment.amount * 10,
      ResNum: payment.res_num,
      MID,
      RedirectURL: `http://localhost:3000/payments/verify/${payment.hash}`
    }
  };
};
