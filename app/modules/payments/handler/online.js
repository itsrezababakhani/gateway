const zarinPal = require("./gateways/zarinpal");
const saman = require("./gateways/saman");

const defaultGateway = "zarinPal";

const gateways = {
  zarinPal,
  saman
};

const generateResult = async gatewayResult => {
  const defaultParams = {
    success: false,
    mustRedirect: false,
    redirectUrl: null,
    mustRenderView: false,
    viewPath: null,
    viewParams: null
  };
  return { ...defaultParams, ...gatewayResult };
};

exports.pay = async payment => {
  const gateway = gateways[defaultGateway];
  const result = await gateway.paymentRequest(payment);
  return await generateResult(result);
};

exports.verify = async (payments, params) => {
  const gatewayResult = await defaultGateway.verify(payments, params);
  return gatewayResult;
};
