const ordersModel = require("../orders/model");
const payment = require("./payment");
const paymentModel = require("./model");

exports.index = async (req, res) => {
  const order_hash = req.params.order_hash;
  const paymentMethod = req.body.payment_method;
  const order = await ordersModel.findOrderByHash(order_hash);

  const {
    success,
    mustRedirect,
    redirectUrl,
    mustRenderView,
    viewPath,
    viewParams
  } = await payment.doPayments(order, paymentMethod);
  if (!success) {
    res.send("not success");
  }

  if (mustRedirect) {
    res.redirect(redirectUrl);
  }

  if (mustRenderView) {
    res.render(viewPath, { layout: "main", params: viewParams });
  }
};

exports.verifyPayments = async (req, res) => {
  const paymentHash = req.params.payment_hash;
  const payments = await paymentModel.findPaymentByHash(paymentHash);
  const verify = await payment.verifyPayment(payments, {
    query: req.query,
    body: req.body
  });
  console.log(verify);
  return verify;
};
