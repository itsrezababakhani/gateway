const microTime = require("microtime");
const connection = require("../../../database/connections/mysql");
const crypto = require("crypto");
exports.generateResNum = () => {
  return microTime.now();
};

exports.create = async payment => {
  const db = await connection();
  payment.hash = crypto.randomBytes(20).toString("hex");
  const [result, fields] = await db.query("INSERT INTO payments SET ?", [
    payment
  ]);
  const [results, field] = await db.query(
    "SELECT * FROM payments WHERE id = ?",
    [result.insertId]
  );

  return results[0];
};

exports.findPaymentByHash = async hash => {
  const db = await connection();
  const [result, fields] = await db.query(
    "SELECT * FROM payments WHERE hash = ?",
    [hash]
  );
  return result[0];
};
