const express = require("express");
const cardController = require("./controller");
const router = express.Router();
router.get("/", cardController.card);
router.get("/add/:slug", cardController.addCard);
module.exports = router;
