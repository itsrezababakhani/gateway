const shopModel = require("../shop/model");
const { productPresentor } = require("./presentor");
exports.card = async (req, res) => {
  const products = req.session.card;
  const productId = Object.keys(products);
  let productItem = productId.map(id => {
    return productPresentor(products[id]);
  });

  res.render("card/list", { layout: "main", product: productItem });
};

exports.addCard = async (req, res) => {
  const slug = req.params.slug;
  const product = await shopModel.getProductBySlug(slug);

  if (!product) {
    res.status(404).send("product not found..!!");
  }

  if (!("card" in req.session)) {
    req.session.card = {};
  }

  if (product.id in req.session.card) {
    let { count } = req.session.card[product.id];
    req.session.card[product.id].count = ++count;
  } else {
    req.session.card[product.id] = {
      id: product.id,
      title: product.title,
      slug: product.slug,
      price: product.price,
      image: product.image,
      count: 1
    };
  }

  res.redirect("/card");
};
