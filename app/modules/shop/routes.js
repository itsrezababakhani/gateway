const express = require("express");
const shopController = require("./controller");
const router = express.Router();
router.get("/", shopController.shop);

module.exports = router;
