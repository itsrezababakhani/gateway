const shopModel = require("./model");
exports.shop = async (req, res) => {
  const products = await shopModel.getProduct();
  res.render("shop/index", { layout: "main", products: products });
};
