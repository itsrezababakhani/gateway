const connection = require("../../../database/connections/mysql");
exports.getProduct = async () => {
  const db = await connection();
  const [results, fields] = await db.query("SELECT * FROM products");
  return results;
};

exports.getProductBySlug = async slug => {
  const db = await connection();
  const [result, fields] = await db.query(
    "SELECT * FROM products WHERE slug =? LIMIT 1",
    slug
  );
  return result[0];
};
