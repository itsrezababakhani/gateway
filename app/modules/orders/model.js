const connection = require("../../../database/connections/mysql");
const crypto = require("crypto");
exports.createOrder = async order => {
  const db = await connection();
  order.order_hash = await crypto.randomBytes(20).toString("hex");
  const [result, fields] = await db.query("INSERT INTO orders SET ?", [order]);
  const [orders, field] = await db.query("SELECT * FROM orders WHERE id=?", [
    result.insertId
  ]);
  return orders[0];
};

exports.findOrderByHash = async hash => {
  const db = await connection();
  const [order, fields] = await db.query(
    "SELECT * FROM orders WHERE order_hash = ?",
    [hash]
  );
  return order[0];
};
