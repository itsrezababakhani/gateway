const ordersModel = require("./model");
exports.createOrder = async (req, res) => {
  const card = req.session.card;
  let order_total = 0;
  Object.keys(card).map(id => {
    order_total = order_total + card[id].total_price;
  });

  const order = {
    order_total,
    order_count: Object.keys(card).length,
    status: 0
  };

  const result = await ordersModel.createOrder(order);
  if (!result) {
    res.status(404).send("not found order");
  }

  res.render("checkout/checkout", { layout: "main", result: result });
};
