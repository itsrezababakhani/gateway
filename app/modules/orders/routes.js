const express = require("express");
const router = express.Router();
const ordersController = require("./controller");
router.get("/", ordersController.createOrder);
module.exports = router;
