const bodyParser = require("body-parser");
const express = require("express");
const session = require("express-session");
const hbs = require("express-handlebars");
const path = require("path");

const middlewares = app => {
  const serverPath = path.dirname(require.main.filename);
  const layoutsPath = path.join(serverPath, "/app/views/layouts");
  const viewsPath = path.join(serverPath, "/app/views");

  app.engine(
    "hbs",
    hbs({
      extname: "hbs",
      layoutsDir: layoutsPath
    })
  );
  app.set("view engine", "hbs");
  app.set("views", viewsPath);
  app.use(express.static(path.join(serverPath, "/public")));

  app.set("trust proxy", 1); // trust first proxy
  app.use(
    session({
      secret: "keyboard cat",
      resave: false,
      saveUninitialized: true,
      cookie: { secure: false }
    })
  );

  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
};

module.exports = middlewares;
