const express = require("express");
require("dotenv").config();
const app = express();

require("./middlewares/middlewares")(app);
require("./router").router(app);

exports.appStart = () => {
  app.listen(process.env.APPLICATION_PORT, () => {
    console.log(`node shop running on port ${process.env.APPLICATION_PORT}`);
  });
};
