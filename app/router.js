const shopRouter = require("./modules/shop/routes");
const cardRouter = require("./modules/card/routes");
const ordersRouter = require("./modules/orders/routes");
const paymentsRouter = require("./modules/payments/routes");
exports.router = app => {
  app.use("/", shopRouter);
  app.use("/card", cardRouter);
  app.use("/orders", ordersRouter);
  app.use("/payments", paymentsRouter);
};
